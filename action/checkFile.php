<?php
function checkFile($dir, $old_name, $new_name, $type) {
    $active = true;
    //When writing incorrect name
    $incorrect = array( ' ', '<', '>', ':', '?', '/', '\\', '|', '*', '"' );
    for ( $i = 0; $i < strlen($new_name); $i++ ) {
        if ( in_array( $new_name[$i], $incorrect ) ) {
            $active = false;
            return 'error';
            break;
        } else {
            $active = true;
        }
    }
    if ( $type == '' ) {
        if ( file_exists($dir.$new_name) ) {
            return 'exists';
        } else {
            return $new_name;
        }
    } else {
        if ( $active ) {
            //checking if file name already exists if yes we're adding a number.
            $newName = $new_name;
            if ( file_exists($dir.$new_name . $type) ) {
                if ( file_exists($dir.$newName.'_1'.$type) ) {
                    for ( $i = 1; file_exists($dir.$newName.'_'.$i.$type) == true; $i++ ) {  
                        $file_ = explode('_', $newName.'_'.$i.$type);
                        $file_end = strToLower(end($file_));
                        $file_1 = explode('.', $file_end);
                        $file_reset = strToLower(reset($file_1));
                        $imageCount = $file_reset + 1;
                        $file_name = $newName . '_'.$imageCount.$type;
                        return $file_name;
                    }
                } else {
                    $file_name = $newName . '_1'.$type;
                    return $file_name;
                }
            } else {
                $file_name = $newName .$type;
                return $file_name;
            }
        }
    }
}
?>