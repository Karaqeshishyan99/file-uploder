<?php 
//Rename item.
if ( isset($_POST['oldName'], $_POST['newName'], $_POST['directory']) ) {
    $dir = $_POST['directory'];
    $old_name = $_POST['oldName'];
    $new_name = $_POST['newName'];
    $type = '';
    if ( is_file($dir . $old_name) ) {
        $fileExt = explode('.', $old_name);
        $fileActualExt = end($fileExt);
        $type = '.'.$fileActualExt;
    }
    if ( file_exists($dir.$old_name) ) {
        require('checkFile.php');
        $new_name = checkFile($dir, $old_name, $new_name, $type);
        if ( $type == '' ) {
            $path = 'dir';
        } else {
            $path = 'file';
        }
        if ( $new_name == 'error' ) {
            $resJson = array( 'error' => true, 'handle' => "can't write these".'( :, ?, <, >, /, \, |, *, " )');
            echo json_encode($resJson);
        } else if ( $new_name == 'exists' ) {
            $resJson = array( 'error' => true, 'path' => $path, 'handle' => 'Folder already exists.' );
            echo json_encode($resJson);
        } else {
            $resJson = array( 'error' => false, 'path' => $path, 'handle' => 'Your file renamed', 'newName' => $new_name );
            echo json_encode($resJson);
            rename( $dir.$old_name, $dir.$new_name );
        }
    }
}
?>