<?php 
//php delete function that deals with directories recursively
function delete_files($target) {
    if(is_dir($target . '/')){
        $files = glob( $target . '/' . '*', GLOB_MARK ); //GLOB_MARK adds a slash to directories returned
        foreach( $files as $file ){
            delete_files( $file );
        }
        rmdir( $target . '/' );
    } else if(is_file($target)) {
        unlink( $target );
    }
}
if ( isset($_POST['path'], $_POST['directory']) && file_exists($_POST['directory'].$_POST['path']) ) {
    $dir = $_POST['directory'];
    $path = $dir . $_POST['path'];
    if (is_dir($path . '/')) {
        echo '<div class="uploaded">Your folder deleted <i class="fas fa-check-circle"></i></div>';
    } else if (is_file($path)) {
        echo '<div class="uploaded">Your file deleted <i class="fas fa-check-circle"></i></div>';
    }
    delete_files($path);
    include_once("../view/content.php");
    // Header("Location:/");
} else {
    include_once("../view/content.php");
}
?>