<?php
//Get sort name and sort value.
if ( isset($_REQUEST['sortBy'], $_REQUEST['sortValue']) ) {
    $sortBy = $_REQUEST['sortBy'];
    $sortValue = $_REQUEST['sortValue'];
} else {
    $sortBy = 'name';
    $sortValue = 'ASC';
}
//Get folders and files size.
function getDirSize($dir, $file) {
    if ( is_dir($dir.$file) ) {
    $dirSize = 0;
    $path = realpath($dir.$file);
    if($path!==false && $path!='' && file_exists($path)){
        foreach(new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path, FilesystemIterator::SKIP_DOTS)) as $object){
                $dirSize += $object->getSize();
        }
    }
        return $dirSize;
    } else {
        return filesize($dir.$file);
    }
}
//With this sort_value function we get sorting with DESC or with ASC.
function sort_value($arrFiles, $value) {
    if ( $value == 'DESC' ) {
        arsort($arrFiles);
        return $arrFiles;
    } 
    else if ( $value == 'ASC' ) {
        asort($arrFiles);
        return $arrFiles;
    }
}
//In this section we get sorting and pushing in fileSort array.
$files = scandir($dir);
$filesSort = array();
//Get sort by name.
if ( $sortBy === 'name' ) {
    $filesSort = sort_value($files, $sortValue);
}
//Get sort by size.
else if ( $sortBy === 'size' ) {
    foreach ( $files as $fs ) {
        if ( $fs != "." && $fs != ".." ) { 
            $arr[$fs] = getDirSize($dir, $fs);
        } else {
            $arr = array();
        }
    }
    $sort = sort_value($arr, $sortValue);
    foreach ( $sort as $fileName => $fileSize ) {
            array_push($filesSort, $fileName);
    }
}
//Get sort by type.
else if ( $sortBy === 'type' ) {
    foreach ( $files as $file ) {
        if ( $file != "." && $file != ".." ) { 
            if( mime_content_type($dir . $file) == 'directory' ) {  
                $ext = 'folder';
            } else {
                $ext = pathinfo($dir . $file, PATHINFO_EXTENSION);
            }
            $arr[$file] = $ext;
        } else {
            $arr = array();
        }
    }
    $sort = sort_value($arr, $sortValue);
    foreach ( $sort as $fileName => $fileSize ) {
            array_push($filesSort, $fileName);
    }
}
?>