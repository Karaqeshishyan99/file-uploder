<?php 
$limit = 4;
$int = 0;
$dir = "images/";
if ( isset($_REQUEST['directory']) ) {
    $dir = $_REQUEST['directory'];
}
if ( file_exists($dir) ) {
    $fi = new FilesystemIterator($dir, FilesystemIterator::SKIP_DOTS);
    $fileCount = iterator_count($fi);
} else {
    $fileCount = 0;
}
$pages = ceil($fileCount / $limit);
$page = 1;
if ( isset($_POST['page']) ) {
    $page = $_POST['page'];
} else if ( isset($_REQUEST['currentPage']) ) {
    $page = $_REQUEST['currentPage'];
}
include_once("bytes.php");
include_once("pagination.php");
include_once("tableItem.php");
include_once('tableGroup.php');
?>