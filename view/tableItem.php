<?php 
//With this tableItem function we get the one file item.
function tableItem($id, $dir, $file) {
    $name;
    if ( mime_content_type($dir . $file) == 'directory' ) {  
        $name = '<a onclick="folderClick(this,event)" href='.$file.'>'.$file.'</a>'; 
        $ext = 'folder';
    } else {  
        $name = $file; 
        $ext = pathinfo($dir . $file, PATHINFO_EXTENSION);
    }
    return '
        <tr class="tableItem">
            <th scope="row">'.$id.'</th>
            <td class="tableItem-name">
                <div class="handle-rename"></div>
                <form onsubmit="form_rename(this, event)" class="form-rename" enctype="multipart/form-data">
                    <input type="text" name="newName" class="input-rename" placeholder="new name" />
                    <button type="submit" name="submit" class="submit submit-rename">Change</button>
                </form>
                <div class="tableItem-title">'.$name.'</div>
            </td>
            <td style="width: 10rem">'.formatSizeUnits(getDirSize($dir, $file)).'</td>
            <td style="width: 10rem">'.$ext.'</td>
            <td>
                <div class="button rename" onclick="rename(this, event)">Rename</div>
            </td>
            <td class="td-delete">
                <a class="button delete" onclick="delate(this, event)" href="?path='.$file.'">Delete</a>
            </td>
        </tr>
    ';
}
?>