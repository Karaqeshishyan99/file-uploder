<?php
//This variables we recieve active classes in sorting.
$sortName = '';
$sortSize = '';
$sortType = '';
if ( isset($_REQUEST['sortBy'], $_REQUEST['sortValue']) ) {
    if ( $_REQUEST['sortValue'] == 'DESC' ) {
        if ( $_REQUEST['sortBy'] == 'name' ) {
            $sortName = 'sort-up';
        } else if ( $_REQUEST['sortBy'] == 'size' ) {
            $sortSize = 'sort-up';
        } else if ( $_REQUEST['sortBy'] == 'type' ) {
            $sortType = 'sort-up';
        }
    } else if ( $_REQUEST['sortValue'] == 'ASC' ) {
        if ( $_REQUEST['sortBy'] == 'name' ) {
            $sortName = 'sort-down';
        } else if ( $_REQUEST['sortBy'] == 'size' ) {
            $sortSize = 'sort-down';
        } else if ( $_REQUEST['sortBy'] == 'type' ) {
            $sortType = 'sort-down';
        }
    }
} else {
    $sortName = 'sort-down';
}
?>
<!--We recieve headings of table -->
<table class="table table-hover table-dark mt-5">
    <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">
                <div class="column-heading <?php echo $sortName; ?>" onclick="sortClick(this, event)">
                    <a href="?sortBy=name">Name</a>
                    <i class="fas fa-caret-down"></i>
                </div>
            </th>
            <th scope="col">
                <div class="column-heading <?php echo $sortSize; ?>" onclick="sortClick(this, event)">
                    <a href="?sortBy=size">Size</a>
                    <i class="fas fa-caret-down"></i>
                </div>
            </th>
            <th scope="col">
                <div class="column-heading <?php echo $sortType; ?>" onclick="sortClick(this, event)">
                    <a href="?sortBy=type">Type</a>
                    <i class="fas fa-caret-down"></i>
                </div>
            </th>
            <th scope='col' colspan="2">Actions</th>
        </tr>
    </thead>
    <tbody>
        <?php
        //Get all files.
        if ( is_dir($dir) ) {
            // $f = scandir($dir);
            // arsort($f);
            if ( $dh = opendir($dir) ) {
                include_once('sortFiles.php');
                foreach ( $filesSort as $file ) {
                    if ( $file != "." && $file != ".." ) {
                        $int++;
                        $start;
                        $end = $int <= $page * $limit;
                        if ( $page == 1 ) {
                            $start = $page;
                        } else {
                            $start = $int > ($page - 1) * $limit;
                        }
                        if ( $end && $start ) {
                            echo tableItem($int, $dir, $file);
                        }
                    }
                }
                closedir($dh);
            }
        }
        ?>
    </tbody>
</table>