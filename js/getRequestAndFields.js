//Get currentpage, directory, and a directory for request fields.
getFields = (currentPage) => {
	var fields = {};
	var urlParams = new URLSearchParams(window.location.search);
	var page = urlParams.get('currentPage');
	fields.page = page;
	if (currentPage) {
		fields.page = currentPage;
	} else {
		if (document.querySelectorAll('.tableItem').length == 1) {
			if (page > 1) {
				fields.page -= 1;
			}
		}
		if (page == null) {
			fields.page = 1;
		}
	}
	var directory = urlParams.get('directory');
	if (directory) {
		fields.requsetDir = `../${directory}`;
		fields.dir = `&directory=${directory}`;
	} else {
		fields.requsetDir = `../images/`;
		fields.dir = '';
	}
	var sortBy = urlParams.get('sortBy');
	var sortValue = urlParams.get('sortValue');
	if (sortBy && sortValue) {
		fields.sortBy = `&sortBy=${sortBy}`;
		fields.sortValue = `&sortValue=${sortValue}`;
	} else {
		fields.sortBy = ``;
		fields.sortValue = ``;
	}
	return fields;
};

//The fetchData functionality is for sending a request and adding a history.
fetchData = (url, xhrHeader, sendData, currentPage, popstate) => {
	document.querySelector('.content').classList.add('content-load');
	var fields = currentPage ? getFields(currentPage) : getFields();
	if (!popstate) {
		window.history.pushState(
			null,
			null,
			`${location.pathname}?currentPage=${fields.page}${fields.dir}${fields.sortBy}${fields.sortValue}`
		);
	}
	const xhr = new XMLHttpRequest();
	xhr.open('POST', url, true);
	xhrHeader.forEach((item) => xhr.setRequestHeader(item.arg1, item.arg2));
	xhr.send(sendData);
	xhr.onreadystatechange = function() {
		if (this.readyState == 4 && xhr.status == 200) {
			document.querySelector('.content').classList.remove('content-load');
			document.querySelector('.content').innerHTML = this.responseText;
			if (document.querySelectorAll('.page-link')[fields.page]) {
				document.querySelectorAll('.page-link')[fields.page].focus();
			}
		}
	};
};
