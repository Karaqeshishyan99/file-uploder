const folder = document.querySelector('.add-folder');
const handle = document.querySelector('.addFolder-handle');
var active = false;
var errorFalse = false;
folder.onsubmit = function(e) {
	e.preventDefault();
	let input = this.folder;
	if (active) {
		function isEmpty(str) {
			return !str.replace(/\s+/, '').length;
		}
		if (!isEmpty(input.value)) {
			const urlParams = new URLSearchParams(window.location.search);
			var myParam = urlParams.get('directory');
			myParam ? myParam : (myParam = 'images/');
			var fields = getFields();
			fetch('addFile-Folder/addFolder.php', {
				method: 'POST',
				body: `folder=${input.value}&directory=../${myParam}${fields.sortBy}${fields.sortValue}`,
				headers: new Headers({
					'Content-type': 'application/x-www-form-urlencoded'
				})
			})
				.then((response) => response.text())
				.then((data) => {
					if (data == 'error') {
						handle.classList.remove('error-false');
						handle.classList.add('error-true');
						input.style.borderColor = '#f80018';
						handle.innerHTML = `can't write these ( :, ?, <, >, /, \, |, *, " ).<i class="fas fa-times-circle"></i>`;
					} else if (data == 'exists') {
						handle.classList.remove('error-false');
						handle.classList.add('error-true');
						input.style.borderColor = '#f80018';
						handle.innerHTML = 'Folder already exists.<i class="fas fa-times-circle"></i>';
					} else {
						active = false;
						errorFalse = true;
						input.value = '';
						handle.classList.remove('error-true');
						handle.classList.add('error-false');
						input.style.borderColor = '#57b846';
						handle.innerHTML = 'folder created.<i class="fas fa-check-circle"></i>';
						document.querySelector('.content').innerHTML = data;
					}
				})
				.catch((error) => console.error(error));
		} else {
			input.style.borderColor = '#f80018';
			handle.classList.remove('error-false');
			handle.classList.add('error-true');
			handle.innerHTML = 'write folder name.<i class="fas fa-times-circle"></i>';
		}
	} else {
		active = true;
		input.classList.add('active-folder');
		input.focus();
	}
};
window.onclick = function(e) {
	if (active || errorFalse) {
		var path = e.path || (e.composedPath && e.composedPath());
		if (!path.includes(folder)) {
			active = false;
			errorFalse = false;
			let input = document.querySelector('.folder');
			input.classList.remove('active-folder');
			handle.classList.remove('error-true');
			handle.classList.remove('error-false');
			input.style.borderColor = '#57b846';
			input.value = '';
			document.querySelector('.addFolder-handle').innerHTML = '';
		}
	}
};
