//With This sortClick function we get sort name and sort Value (ASC, DESC).
let sortUp = true;
let sortDown = false;
let sortBy;
let sortValue;
sortClick = (element, e) => {
	e.preventDefault();
	let elements = document.querySelectorAll('.column-heading');
	let iconIndex;
	let iconsArray = [];
	for (let columnHeading of element.children) {
		if (columnHeading.href) {
			var urlParams = new URLSearchParams(columnHeading.search);
			sortBy = urlParams.get('sortBy');
		}
	}
	elements.forEach((elem, index) => {
		iconsArray.push(elem);
		if (element.classList[1] == 'sort-up' || element.classList[1] == 'sort-up-anim') {
			sortDown = true;
			sortUp = false;
		}
	});
	if (sortUp || sortDown === false) {
		sortUp = false;
		sortValue = 'DESC';
	} else {
		sortUp = true;
		sortDown = false;
		sortValue = 'ASC';
	}
	//In this section we send request and get sorting fileds.
	var fields = getFields();
	window.history.pushState(
		null,
		null,
		`${location.pathname}?currentPage=${fields.page}${fields.dir}&sortBy=${sortBy}&sortValue=${sortValue}`
	);
	fetch('view/content.php', {
		method: 'POST',
		body: `currentPage=${fields.page}&directory=${fields.requsetDir}&sortBy=${sortBy}&sortValue=${sortValue}`,
		headers: new Headers({
			'Content-type': 'application/x-www-form-urlencoded'
		})
	})
		.then((response) => response.text())
		.then((data) => {
			document.querySelector('.content').innerHTML = data;
		})
		.then(() => {
			iconIndex = iconsArray.indexOf(element);
			var urlParams = new URLSearchParams(window.location.search);
			let getSortValue = urlParams.get('sortValue');
			if (getSortValue == 'ASC') {
				document.querySelectorAll('.column-heading ')[iconIndex].classList.remove('sort-up');
				document.querySelectorAll('.column-heading ')[iconIndex].classList.remove('sort-up-anim');
			} else if (getSortValue == 'DESC') {
				document.querySelectorAll('.column-heading ')[iconIndex].classList.remove('sort-up');
				document.querySelectorAll('.column-heading ')[iconIndex].classList.add('sort-up-anim');
			}
		})
		.catch((error) => console.error(error));
	// var promise = new Promise((resolve, reject) => {
	// 	resolve('request');
	// });
	// console.log(promise);
	// promise
	// 	.then((result) => {
	// 		return new Promise(function(resolve, reject) {
	// 			window.history.pushState(null, null, `?sortBy=${sortBy}&sortValue=${sortValue}`);
	// 			var fields = getFields();
	// 			let xhrHeader = [ { arg1: 'Content-type', arg2: 'application/x-www-form-urlencoded' } ];
	// 			fetchData(
	// 				'view/content.php',
	// 				xhrHeader,
	// 				`currentPage=${fields.page}&directory=${fields.requsetDir}&sortBy=${sortBy}&sortValue=${sortValue}`,
	// 				false,
	// 				false,
	// 				icon
	// 			);
	// 			resolve('response');
	// 		});
	// 	})
	// 	.then((result) => {
	// 		if (result == 'response') {
	// 			console.log(icon);
	// 			var urlParams = new URLSearchParams(window.location.search);
	// 			let getSortValue = urlParams.get('sortValue');
	// 			if (getSortValue == 'ASC') {
	// 				icon.classList.add('sort-down');
	// 			} else if (getSortValue == 'DESC') {
	// 				document.querySelector('.fa-caret-down').classList.add('sort-up');
	// 			}
	// 		}
	// 	});
};
