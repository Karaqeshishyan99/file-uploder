//When is click on the browser back or forward buttons will work onpopstate function
window.onpopstate = function() {
	var fields = getFields();
	let xhrHeader = [ { arg1: 'Content-type', arg2: 'application/x-www-form-urlencoded' } ];
	//with this fetchData function sends request
	fetchData(
		'view/content.php',
		xhrHeader,
		`page=${fields.page}&directory=${fields.requsetDir}${fields.sortBy}${fields.sortValue}${fields.sortBy}${fields.sortValue}`,
		(page = false),
		(popstate = true)
	);
	//With this breadcrumb function creating the current page location within a navigation
	breadcrumb();
};

//Pagination functional
pagination = (element, e) => {
	e.preventDefault();
	var url = new URL(element.href);
	var page = url.searchParams.get('page');
	if (page == 'min' || page == 'max') {
		console.log('Not');
	} else {
		var fields = getFields(page);
		let xhrHeader = [ { arg1: 'Content-type', arg2: 'application/x-www-form-urlencoded' } ];
		fetchData(
			'view/content.php',
			xhrHeader,
			`page=${fields.page}&directory=${fields.requsetDir}${fields.sortBy}${fields.sortValue}`,
			page
		);
	}
};

//Upload file functional
if (document.querySelector('#upload-form')) {
	const uploadForm = document.querySelector('#upload-form');
	uploadForm.onsubmit = function(e) {
		e.preventDefault();
		var file = document.querySelector('#file').files[0];
		console.log(file);
		var form_data = new FormData();
		form_data.append('file', file);
		var fields = getFields();
		form_data.append('currentPage', fields.page);
		form_data.append('directory', fields.requsetDir);
		var urlParams = new URLSearchParams(window.location.search);
		var sortBy = urlParams.get('sortBy');
		var sortValue = urlParams.get('sortValue');
		if (sortBy && sortValue) {
			form_data.append('sortBy', sortBy);
			form_data.append('sortValue', sortValue);
		}
		if (file !== undefined) {
			let xhrHeader = [
				{ arg1: 'Cache-Control', arg2: 'no-cache' },
				{ arg1: 'X-Requested-With', arg2: 'XMLHttpRequest' }
			];
			fetchData('addFile-Folder/upload.php', xhrHeader, form_data);
		}
	};
}

//Delete file functional.
delate = (element, e) => {
	e.preventDefault();
	const urlElement = new URL(element.href);
	var path = urlElement.searchParams.get('path');
	var fields = getFields();
	let xhrHeader = [ { arg1: 'Content-type', arg2: 'application/x-www-form-urlencoded' } ];
	fetchData(
		'action/delate.php',
		xhrHeader,
		`path=${path}&currentPage=${fields.page}&directory=${fields.requsetDir}${fields.sortBy}${fields.sortValue}`
	);
};

//rename button functional. When click will open form rename
var handle_rename;
var old_name;
var old_href;
rename = (element, e) => {
	e.preventDefault();
	let allForms = document.querySelectorAll('.form-rename');
	allForms.forEach(function(elem) {
		elem.style.display = 'none';
	});
	e.path.forEach(function(elem) {
		if (elem.className == 'tableItem') {
			for (let cardChild of elem.children) {
				if (cardChild.className == 'tableItem-name') {
					for (let cardBodyChild of cardChild.children) {
						if (cardBodyChild.className == 'handle-rename') {
							handle_rename = cardBodyChild;
						}
						if (cardBodyChild.className == 'form-rename') {
							cardBodyChild.style.display = 'block';
							cardBodyChild.children[0].focus();
						}
						if (cardBodyChild.className == 'tableItem-title') {
							old_name = cardBodyChild;
						}
					}
				}
				if (cardChild.className == 'td-delete') {
					old_href = cardChild.children[0];
				}
			}
		}
	});
};

//Render functional for rename
function render(response) {
	handle_rename.classList.remove('erorr-rename');
	handle_rename.classList.add('change-rename');
	handle_rename.innerHTML = response.handle;
	if (response.path == 'dir') {
		old_name.innerHTML = `<a onclick="folderClick(this,event)" href=${response.newName}>${response.newName}</a>`;
	} else if (response.path == 'file') {
		old_name.innerHTML = response.newName;
	}
	let href = old_href.href.replace(`${old_href}`, `?path=${response.newName}`);
	old_href.href = href;
}

//Form rename functional
form_rename = (element, e) => {
	e.preventDefault();
	var newName = element[0].value;
	var oldName = old_name.innerText;
	var fields = getFields();
	const xhr = new XMLHttpRequest();
	xhr.open('POST', 'action/rename.php', true);
	xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
	xhr.responseType = 'json';
	xhr.send(`oldName=${oldName}&newName=${newName}&directory=${fields.requsetDir}${fields.sortBy}${fields.sortValue}`);
	xhr.onreadystatechange = function() {
		if (this.readyState == 4 && xhr.status == 200) {
			const data = this.response;
			if (data.error) {
				handle_rename.classList.remove('change-rename');
				handle_rename.classList.add('erorr-rename');
				handle_rename.innerText = data.handle;
			} else {
				render(data);
				element.style.display = 'none';
				if (document.querySelector('.uploaded')) {
					document.querySelector('.uploaded').parentNode.removeChild(document.querySelector('.uploaded'));
				}
				if (document.querySelector('.error-upload')) {
					document
						.querySelector('.error-upload')
						.parentNode.removeChild(document.querySelector('.error-upload'));
				}
			}
		}
	};
};
