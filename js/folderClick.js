//With this breadcrumb function creating the current page location within a navigation
breadcrumb = () => {
	const breadcrumb = document.querySelector('.breadcrumb');
	breadcrumb.innerHTML = '';
	const urlParams = new URLSearchParams(window.location.search);
	var myParam = urlParams.get('directory');
	myParam ? (myParam = myParam.split('/', 99999)) : (myParam = [ 'images', '' ]);
	var items = myParam.map((item, index) => {
		if (!item == '') {
			let newElem = document.createElement('Li');
			if (myParam.length - 2 == index) {
				newElem.setAttribute('class', 'breadcrumb-item active');
				let name = document.createTextNode(item);
				newElem.appendChild(name);
				breadcrumb.appendChild(newElem);
			} else {
				newElem.setAttribute('class', 'breadcrumb-item');
				let newLink = document.createElement('a');
				newLink.setAttribute('href', item);
				newLink.setAttribute('onclick', 'breadcrumbClick(this, event)');
				let name = document.createTextNode(item);
				newLink.appendChild(name);
				newElem.appendChild(newLink);
				breadcrumb.appendChild(newElem);
			}
		}
	});
};
fetchRequest = (directory) => {
	fetch('view/content.php', {
		method: 'POST',
		body: directory,
		headers: new Headers({
			'Content-type': 'application/x-www-form-urlencoded'
		}),
		mode: 'cors'
	})
		.then((response) => response.text())
		.then((text) => {
			breadcrumb();
			document.querySelector('.content').innerHTML = text;
		})
		.catch((error) => console.error(error));
};

//Navigation folder click functional.
breadcrumbClick = (element, event) => {
	event.preventDefault();
	const urlParams = new URLSearchParams(window.location.search);
	var myParam = urlParams.get('directory');
	let dirArr = myParam.split(element.innerText, 1);
	let newDir = dirArr + element.innerText + '/';
	let directory = `directory=../${newDir}`;
	window.history.pushState(null, null, `${location.pathname}?directory=${newDir}`);
	fetchRequest(directory);
};

breadcrumb();

//Folder click functional.
folderClick = (element, event) => {
	event.preventDefault();
	let elemHref = element.innerText;
	let href;
	let directory;
	var url_string = window.location.href;
	var url = new URL(url_string);
	var dir = url.searchParams.get('directory');
	if (dir) {
		directory = `directory=../${dir}${elemHref}/`;
		href = `?directory=${dir}${elemHref}/`;
	} else {
		directory = `directory=../images/${elemHref}/`;
		href = `?directory=images/${elemHref}/`;
	}
	window.history.pushState(null, null, `${location.pathname}${href}`);
	fetchRequest(directory);
};
