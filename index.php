<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>File uploader</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css"
        integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="css/variables.css" />
    <link rel="stylesheet" type="text/css" href="css/index.css" />
</head>

<body>
    <section class="section">
        <div class="container">
            <div class="row justify-content-between mx-0">
                <div class="row align-items-baseline mx-0">
                    <form enctype="multipart/form-data" id="upload-form">
                        <label for="file" class="button file-button">
                            <i class="fas fa-cloud-upload-alt"></i>Choose your file
                        </label>
                        <input type="file" name="file" id="file" />
                        <button type="submit" name="submit" class="submit">
                            <i class="fas fa-plus"></i>Add image
                        </button>
                    </form>
                    <form enctype="multipart/form-data" class="add-folder">
                        <button type="submit" name="submit" class="submit ml-3">
                            <i class="fas fa-folder-plus"></i>Add folder
                        </button>
                        <input type="text" name="folder" placeholder="Folder name" class="folder" />
                        <label class="addFolder-handle"></label>
                    </form>
                </div>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb"></ol>
                </nav>
            </div>
            <div class="content">
                <?php include_once("view/content.php") ?>
            </div>
        </div>
    </section>
    <script src="js/getRequestAndFields.js"></script>
    <script src="js/index.js"></script>
    <script src="js/addFolder.js"></script>
    <script src="js/folderClick.js"></script>
    <script src="js/sortFiles.js"></script>
</body>

</html>