<?php
//File Upload
if ( isset($_FILES['file']) ) {
    if (!file_exists('../images')) {
        mkdir('../images', 0666, true);
    }
    if ( isset($_POST['directory']) ) {
        $folderDir = $_POST['directory'];
    }
    //Get file attributes.
    $fileName = $_FILES['file']['name'];
    $fileType = $_FILES['file']['type'];
    $fileSize = $_FILES['file']['size'];
    $fileTmp = $_FILES['file']['tmp_name'];
    $fileError = $_FILES['file']['error'];

    $fileExt = explode('.', $fileName);
    $fileActualExt = strToLower(end($fileExt));
    $alLowed = array('jpg', 'jpeg', 'png', 'gif');
    if ( $fileError === 0 ) {
        if ( $fileSize < 1 * 1024 * 1024 * 1024 ) {
            $imageName = strToLower(reset($fileExt));
            $file_name;
            //checking if file name already exists if yes we're adding a number.
            if ( file_exists($folderDir.$fileName) ) {
                if ( file_exists($folderDir.$imageName.'_1.'.$fileActualExt) ) {
                    for ( $i = 1; file_exists($folderDir.$imageName.'_'.$i.'.'.$fileActualExt) == true; $i++ ) {  
                        $file_ = explode('_', $imageName.'_'.$i.'.'.$fileActualExt);
                        $file_end = strToLower(end($file_));
                        $file_1 = explode('.', $file_end);
                        $file_reset = strToLower(reset($file_1));
                        $imageCount = $file_reset + 1;
                        $file_name = $imageName . '_'.$imageCount.'.'.$fileActualExt;
                    }
                } else {
                    $file_name = $imageName . '_1.'.$fileActualExt;
                }
            } else {
                $file_name = $imageName .'.'.$fileActualExt;
            }
            $fileUpload = $folderDir . $file_name;
            move_uploaded_file($fileTmp, $fileUpload);
            echo '<div class="uploaded">Your file uploaded <i class="fas fa-check-circle"></i></div>';
            include_once("../view/content.php");
        } else {
            echo '<div class="error-upload">Your file is big <i class="fas fa-times-circle"></i></div>';
            include_once("../view/content.php");
        }
    } else {
        echo $fileType;
        echo '<div class="error-upload">There was error uploading your file <i class="fas fa-times-circle"></i></div>';
        include_once("../view/content.php");
    }
} else {
    include_once("../view/content.php");
}
?>