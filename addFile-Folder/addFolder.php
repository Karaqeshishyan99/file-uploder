<?php 
if ( isset($_POST['folder'], $_POST['directory']) ) {
    $dir = $_POST['directory'];
    $old_name = '';
    $new_name = $_POST['folder'];
    $type = '';
    require('../action/checkFile.php');
    $new_name = checkFile($dir, $old_name, $new_name, $type);
    if ( $new_name == 'error' ) {
        echo 'error'; 
    } else if ( $new_name == 'exists' ) {
        echo 'exists';
    } else {
        mkdir($dir.$new_name, 0777, true);
        include('../view/content.php');
    }
}
?>